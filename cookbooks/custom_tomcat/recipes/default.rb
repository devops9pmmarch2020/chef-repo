#
# Cookbook:: custom_tomcat
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

package 'java-1.8.0-openjdk' do
 action :install
end

package 'java-1.8.0-openjdk-devel' do
 action :install
end

user node['tomcat']['user'] do
  comment node['tomcat']['user']
  home node['tomcat']['homedir']
  system true
  shell '/bin/nologin'
end

group node['tomcat']['group'] do
  action :create
end

package 'wget' do
 action :install
end

ark node['tomcat']['user'] do
  url node['tomcat']['tomcaturl']
  home_dir node['tomcat']['homedir']
  prefix_root node['tomcat']['prefix_root']
  owner node['tomcat']['user']
  version node['tomcat']['tomcatversion']
end

execute 'GivingPermisions' do
 command 'chmod 755 /opt/tomcat/bin/*.sh'
end

template "/opt/tomcat/conf/tomcat-users.xml" do
  source 'tomcat-users.xml.erb'
  owner node['tomcat']['user']
  group node['tomcat']['group']
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/opt/tomcat/webapps/manager/META-INF/context.xml" do
  source 'manager-context.xml.erb'
  owner node['tomcat']['user']
  group node['tomcat']['group']
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/etc/systemd/system/tomcat.service" do
  source 'tomcat-init.erb'
  owner node['tomcat']['rootuser']
  group node['tomcat']['rootgroup']
  mode '0755'
  notifies :restart, 'service[tomcat]', :delayed
end

service node['tomcat']['user'] do
  supports :restart => true, :start => true, :stop => true 
  action [ :enable, :start ]
end




